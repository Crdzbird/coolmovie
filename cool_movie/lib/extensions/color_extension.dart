import 'dart:ui';

extension ColorExtension on Color {
  String get toHex => '#${value.toRadixString(16).substring(2)}';
}
