import 'package:cool_movie/models/error/failure.dart';

extension FailureExtension on Failure {
  String get message => switch (runtimeType) {
        ServerFailure => (this as ServerFailure).errorMessage,
        _ => 'unknown error',
      };
}
