import 'package:cool_movie/blocs/theme/theme_cubit.dart';
import 'package:cool_movie/extensions/target_platform_extension.dart';
import 'package:cool_movie/global/globals.dart';
import 'package:cool_movie/l10n/l10n.dart';
import 'package:cool_movie/models/enums/font_type.dart';
import 'package:cool_movie/models/enums/standard_color_type.dart';
import 'package:cool_movie/styles/typography.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:routemaster/routemaster.dart';

/// Extension on [BuildContext] to provide convenient methods for theme,
/// device type checking, and showing platform-specific snackbars.
///
/// This extension adds multiple utility properties and method to [BuildContext]
/// to enhance its functionality in a Flutter application. It includes methods
/// for getting theme data, determining the device type, orientation,
/// and showing platform-specific snackbars.
///
/// Example Usage:
/// ```dart
/// // To get light theme data
/// ThemeData lightTheme = context.light;
///
/// // To check if the device is a tablet
/// bool isTablet = context.isTablet;
///
/// // To show a platform-specific snackbar
/// context.platformSnackbar(title: 'Hello', message: 'This is a message');
/// ```
///
/// See also:
///
///  * [BuildContext], which is the base class for this extension.
///  * [ThemeData], which is used to provide theme information.
///  * [MediaQuery], which is used for device size and orientation information.
extension BuildContextExtension on BuildContext {
  Routemaster get routemaster => Routemaster.of(this);

  /// Provides light theme data for the current context.
  ThemeData get light => ThemeData(
        useMaterial3: true,
        platform: defaultTargetPlatform,
        colorScheme: Globals.schemeLight,
        fontFamily: FontType.sfPro.name,
        textTheme: typographyLightTheme,
        appBarTheme: AppBarTheme(
          backgroundColor: StandardColorsType.transparent.color,
          centerTitle: false,
          actionsIconTheme: IconThemeData(
            color: StandardColorsType.white.color,
          ),
          elevation: 0,
          iconTheme: IconThemeData(
            color: StandardColorsType.white.color,
          ),
          titleTextStyle: typographyLightTheme.titleLarge,
        ),
        inputDecorationTheme: InputDecorationTheme(
          hintStyle: typographyLightTheme.labelMedium,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: StandardColorsType.redish.color),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: StandardColorsType.white.color),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: StandardColorsType.redish.color),
            borderRadius: BorderRadius.circular(8),
          ),
        ),
        progressIndicatorTheme: ProgressIndicatorThemeData(
          color: StandardColorsType.white.color,
        ),
        checkboxTheme: CheckboxThemeData(
          fillColor: MaterialStateProperty.resolveWith(
            (states) => StandardColorsType.white.color,
          ),
          checkColor: MaterialStateProperty.resolveWith(
            (states) => StandardColorsType.black.color,
          ),
        ),
        bottomSheetTheme: BottomSheetThemeData(
          backgroundColor: StandardColorsType.darkBlue.color,
        ),
        tabBarTheme: TabBarTheme(
          labelColor: StandardColorsType.white.color,
          unselectedLabelColor: StandardColorsType.white.color,
          dividerColor: StandardColorsType.redish.color,
          dividerHeight: 1,
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(
              color: StandardColorsType.white.color,
              width: 2,
            ),
            insets: const EdgeInsets.symmetric(horizontal: 16),
          ),
          indicatorSize: TabBarIndicatorSize.tab,
        ),
        iconTheme: IconThemeData(color: StandardColorsType.white.color),
        dividerTheme: DividerThemeData(
          color: StandardColorsType.redish.color,
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: StandardColorsType.blackSemiTranslucid.color,
          selectedItemColor: StandardColorsType.white.color,
          unselectedItemColor: StandardColorsType.white.color,
          selectedLabelStyle: typographyLightTheme.labelSmall,
          unselectedLabelStyle: typographyLightTheme.labelSmall,
          type: BottomNavigationBarType.fixed,
          elevation: 0,
        ),
        iconButtonTheme: IconButtonThemeData(
          style: IconButton.styleFrom(
            backgroundColor: StandardColorsType.redish.color,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
          ),
        ),
        filledButtonTheme: FilledButtonThemeData(
          style: FilledButton.styleFrom(
            backgroundColor: StandardColorsType.redish.color,
          ),
        ),
      );

  /// Provides dark theme data for the current context.
  ThemeData get dark => ThemeData(
        useMaterial3: true,
        platform: defaultTargetPlatform,
        colorScheme: Globals.schemeDark,
        fontFamily: FontType.sfPro.name,
        textTheme: typographyDarkTheme,
        appBarTheme: AppBarTheme(
          backgroundColor: StandardColorsType.transparent.color,
          centerTitle: false,
          actionsIconTheme: IconThemeData(
            color: StandardColorsType.white.color,
          ),
          elevation: 0,
          iconTheme: IconThemeData(
            color: StandardColorsType.white.color,
          ),
          titleTextStyle: typographyDarkTheme.titleLarge,
        ),
        inputDecorationTheme: InputDecorationTheme(
          hintStyle: typographyDarkTheme.labelMedium,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: StandardColorsType.redish.color),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: StandardColorsType.white.color),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: StandardColorsType.redish.color),
            borderRadius: BorderRadius.circular(8),
          ),
        ),
        progressIndicatorTheme: ProgressIndicatorThemeData(
          color: StandardColorsType.white.color,
        ),
        checkboxTheme: CheckboxThemeData(
          fillColor: MaterialStateProperty.resolveWith(
            (states) => StandardColorsType.white.color,
          ),
          checkColor: MaterialStateProperty.resolveWith(
            (states) => StandardColorsType.black.color,
          ),
        ),
        bottomSheetTheme: BottomSheetThemeData(
          backgroundColor: StandardColorsType.darkBlue.color,
        ),
        tabBarTheme: TabBarTheme(
          labelColor: StandardColorsType.white.color,
          unselectedLabelColor: StandardColorsType.white.color,
          dividerColor: StandardColorsType.redish.color,
          dividerHeight: 1,
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(
              color: StandardColorsType.white.color,
              width: 2,
            ),
            insets: const EdgeInsets.symmetric(horizontal: 16),
          ),
          indicatorSize: TabBarIndicatorSize.tab,
        ),
        iconTheme: IconThemeData(color: StandardColorsType.white.color),
        dividerTheme: DividerThemeData(
          color: StandardColorsType.redish.color,
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: StandardColorsType.blackSemiTranslucid.color,
          selectedItemColor: StandardColorsType.white.color,
          unselectedItemColor: StandardColorsType.white.color,
          selectedLabelStyle: typographyDarkTheme.labelSmall,
          unselectedLabelStyle: typographyDarkTheme.labelSmall,
          type: BottomNavigationBarType.fixed,
          elevation: 0,
        ),
        iconButtonTheme: IconButtonThemeData(
          style: IconButton.styleFrom(
            backgroundColor: StandardColorsType.redish.color,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
          ),
        ),
        filledButtonTheme: FilledButtonThemeData(
          style: FilledButton.styleFrom(
            backgroundColor: StandardColorsType.redish.color,
          ),
        ),
      );

  /// Shows a platform-specific snackbar based on the current operating system.
  ///
  /// For Unix platforms or when [isToast] is `true`, it shows a simple message.
  /// On other platforms, it shows a Cupertino-style action sheet.
  ///
  /// Parameters:
  /// - [title]: The title of the snackbar.
  /// - [message]: The optional message body of the snackbar.
  /// - [isToast]: A flag to force a simple toast-style message.
  void platformSnackbar({
    required String title,
    String? message,
    bool isToast = false,
  }) {
    if (!defaultTargetPlatform.isMacOS || isToast) {
      _showMessage(title, message);
      return;
    }
    showCupertinoModalPopup<void>(
      context: this,
      builder: (modalContext) => CupertinoActionSheet(
        title: Text(
          title,
          style: Theme.of(this).textTheme.labelLarge,
          textAlign: TextAlign.center,
        ),
        message: message != null
            ? Text(
                message,
                style: Theme.of(this).textTheme.labelSmall,
                textAlign: TextAlign.center,
              )
            : null,
        actions: <Widget>[
          CupertinoActionSheetAction(
            child: Text(l10n.ok),
            onPressed: () {
              Navigator.pop(this);
            },
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          onPressed: () => Navigator.pop(this),
          child: Text(l10n.cancel),
        ),
      ),
    );
  }

  /// Returns `true` if the current theme mode is dark.
  bool get isDark {
    final currentTheme =
        select((ThemeCubit themeCubit) => themeCubit.state).theme;
    if (currentTheme == ThemeMode.system) {
      return Theme.of(this).brightness == Brightness.dark;
    }
    return currentTheme == ThemeMode.dark;
  }

  /// Returns `true` if the current theme mode is light.
  bool get isLight {
    final currentTheme =
        select((ThemeCubit themeCubit) => themeCubit.state).theme;
    if (currentTheme == ThemeMode.system) {
      return Theme.of(this).brightness == Brightness.light;
    }
    return currentTheme == ThemeMode.light;
  }

  /// A private method to show a simple message snackbar.
  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _showMessage(
    String title,
    String? message,
  ) =>
      Globals.rootScaffoldMessengerKey.currentState!.showSnackBar(
        SnackBar(
          showCloseIcon: true,
          content: Text(
            title,
            textAlign: TextAlign.center,
          ),
          margin: const EdgeInsetsDirectional.only(
            bottom: 10,
            start: 10,
            end: 10,
          ),
          behavior: SnackBarBehavior.floating,
        ),
      );
}
