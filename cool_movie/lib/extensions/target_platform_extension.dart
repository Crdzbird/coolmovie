import 'package:flutter/cupertino.dart';

/// Extension on [TargetPlatform] to provide additional platform-related properties.
///
/// This extension enhances the [TargetPlatform] enum from the Flutter framework,
/// providing a convenient way to check if the current platform is macOS or iOS.
///
/// Example Usage:
/// ```dart
/// TargetPlatform platform = TargetPlatform.iOS;
///
/// // Check if the platform is macOS or iOS
/// bool isAppleOS = platform.isMacOS;
/// ```
///
/// See also:
///
///  * [TargetPlatform], an enum from the Flutter framework representing different target platforms.
extension TargetPlatformExtension on TargetPlatform {
  /// Checks if the current platform is either macOS or iOS.
  ///
  /// This property compares the current [TargetPlatform] against iOS and macOS
  /// and returns `true` if the platform is either of the two.
  ///
  /// Returns `true` if the platform is macOS or iOS, `false` otherwise.
  bool get isMacOS =>
      this == TargetPlatform.iOS || this == TargetPlatform.macOS;
}
