import 'dart:ui';

extension StringExtension on String {
  bool get isNullOrEmpty => isEmpty || this == '';

  Color get toColor => Color(int.parse('0xff$this'));

  Color get toColorFromHex {
    final hexColor = replaceAll('#', '');
    return Color(int.parse('0xff$hexColor'));
  }

  String generateUUID() {
    final uuid = StringBuffer();
    for (var i = 0; i < length; i++) {
      if (i == 8 || i == 12 || i == 16 || i == 20) {
        uuid.write('-');
      }
      uuid.write(this[i]);
    }
    return uuid.toString();
  }
}
