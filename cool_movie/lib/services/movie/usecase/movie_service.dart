import 'dart:convert';

import 'package:cool_movie/models/enums/preferences_type.dart';
import 'package:cool_movie/models/error/failure.dart';
import 'package:cool_movie/models/movie/movie.dart';
import 'package:cool_movie/models/movie_detail/movie_detail.dart';
import 'package:cool_movie/models/movie_review/movie_review.dart';
import 'package:cool_movie/services/graphql/client.dart';
import 'package:cool_movie/services/graphql/queries.dart';
import 'package:cool_movie/services/movie/abstract/movie_service_abstract.dart';
import 'package:cool_movie/services/preferences/preferences.dart';
import 'package:dartz/dartz.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

final class MovieService extends MovieServiceAbstract {
  MovieService(this._client, this._preferences);
  final Client _client;
  final Preferences _preferences;
  @override
  Future<Either<Failure, List<Movie>>> getMovies() async {
    final cache = await _preferences.read(key: PreferencesType.movies.name);
    try {
      final result = await _client.query(
        QueryOptions(
          document: gql(
            Queries.fetchMovies,
          ),
        ),
      );
      if (result.hasException) {
        if (cache != null) {
          return Right(
            Movie.fromJsonList(
              json.decode(cache) as Map<String, dynamic>,
            ),
          );
        }
        return Left(ServerFailure(errorMessage: result.exception.toString()));
      }
      await _preferences.write(
        key: PreferencesType.movies.name,
        value: json.encode(result.data),
      );
      return Right(
        Movie.fromJsonList(result.data ?? {}),
      );
    } catch (e) {
      if (cache != null) {
        return Right(
          Movie.fromJsonList(
            json.decode(cache) as Map<String, dynamic>,
          ),
        );
      }
      return Left(ServerFailure(errorMessage: e.toString()));
    }
  }

  @override
  Future<Either<Failure, MovieDetail>> getMovieDetail(String id) async {
    final cache =
        await _preferences.read(key: PreferencesType.movieDetail.name);
    try {
      final result = await _client.query(
        QueryOptions(
          document: gql(
            Queries.fetchMovieDetail,
          ),
          variables: {
            'id': id,
          },
        ),
      );
      if (result.hasException) {
        if (cache != null) {
          return Right(
            MovieDetail.fromMap(
              json.decode(cache) as Map<String, dynamic>,
            ),
          );
        }
        return Left(ServerFailure(errorMessage: result.exception.toString()));
      }
      await _preferences.write(
        key: PreferencesType.movieDetail.name,
        value: json.encode(result.data!['movieById']),
      );
      return Right(
        MovieDetail.fromMap(
          result.data?['movieById'] as Map<String, dynamic>? ?? {},
        ),
      );
    } catch (e) {
      if (cache != null) {
        return Right(
          MovieDetail.fromMap(
            json.decode(cache) as Map<String, dynamic>,
          ),
        );
      }
      return Left(ServerFailure(errorMessage: e.toString()));
    }
  }

  @override
  Future<Either<Failure, void>> removeReview(String id) async {
    final result = await _client.mutate(
      MutationOptions(
        document: gql(
          Queries.deleteMovieReviewById,
        ),
        variables: {
          'id': id,
        },
      ),
    );
    if (result.hasException) {
      return Left(ServerFailure(errorMessage: result.exception.toString()));
    }
    return const Right(null);
  }

  @override
  Future<Either<Failure, void>> addReview(MovieReview review) async {
    final result = await _client.mutate(
      MutationOptions(
        document: gql(
          Queries.createMovieReviewMutation,
        ),
        variables: {
          'input': review.toMapCreate,
        },
      ),
    );
    if (result.hasException) {
      return Left(ServerFailure(errorMessage: result.exception.toString()));
    }
    return const Right(null);
  }
}
