import 'package:cool_movie/models/error/failure.dart';
import 'package:cool_movie/models/movie/movie.dart';
import 'package:cool_movie/models/movie_detail/movie_detail.dart';
import 'package:cool_movie/models/movie_review/movie_review.dart';
import 'package:dartz/dartz.dart';

abstract class MovieServiceAbstract {
  Future<Either<Failure, List<Movie>>> getMovies();
  Future<Either<Failure, MovieDetail>> getMovieDetail(String id);
  Future<Either<Failure, void>> removeReview(String id);
  Future<Either<Failure, void>> addReview(MovieReview review);
}
