import 'package:shared_preferences/shared_preferences.dart';

/// Wrapper class for the shared preferences.
class Preferences {
  Preferences({required this.sharedPreferences});
  final SharedPreferences sharedPreferences;

  /// Encrypts and writes the [value] to the shared preferences.
  Future<void> write({required String key, required String value}) {
    return sharedPreferences.setString(key, value);
  }

  /// Encrypts and writes the [values] to the shared preferences.
  Future<void> writeList({required String key, required List<String> values}) =>
      sharedPreferences.setStringList(key, values);

  /// Reads and decrypts the value from the shared preferences.
  Future<String?> read({required String key}) async {
    return sharedPreferences.getString(key);
  }

  /// Reads and decrypts the list of values from the shared preferences.
  Future<List<String>?> readList({required String key}) async =>
      sharedPreferences.getStringList(key);

  /// Clears the shared preferences.
  Future<bool> clearAll() async => sharedPreferences.clear();

  Future<bool> clear({required String key}) async =>
      sharedPreferences.remove(key);
}
