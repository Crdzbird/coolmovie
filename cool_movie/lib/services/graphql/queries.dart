final class Queries {
  static const fetchMovies = '''
query AllMovies {
    allMovies {
      nodes {
        id
        title
        imgUrl
      }
    }
  }''';

  static const fetchMovieDetail = r'''
  query MovieById($id: UUID!) {
    movieById(id: $id) {
      id
      imgUrl
      releaseDate
      title
      movieReviewsByMovieId {
        nodes {
          id
          body
          rating
          title
          userByUserReviewerId {
            name
            id
          }
        }
      }
    }
  }
''';

  static const deleteMovieReviewById = r'''
  mutation DeleteMovieReviewById($id: UUID!) {
    deleteMovieReviewById(input: {id: $id}) {
      deletedMovieReviewId
    }
  }
''';

  static const createMovieReviewMutation = r'''
  mutation CreateMovieReview($input: CreateMovieReviewInput!) {
    createMovieReview(input: $input) {
      movieReview {
        id
        title
        body
        rating
        movieByMovieId {
          title
        }
        userByUserReviewerId {
          name
        }
      }
    }
  }
''';
}
