import 'package:graphql_flutter/graphql_flutter.dart';

class Client extends GraphQLClient {
  Client(String url)
      : super(link: HttpLink(url), cache: GraphQLCache(store: InMemoryStore()));
}
