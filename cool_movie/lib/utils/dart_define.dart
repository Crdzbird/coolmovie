class DartDefine {
  factory DartDefine() => _self;

  DartDefine._internal();

  static final DartDefine _self = DartDefine._internal();

  String get graphqlURL => const String.fromEnvironment('graphqlURL');
}
