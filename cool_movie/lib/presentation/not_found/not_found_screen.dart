import 'package:cool_movie/l10n/l10n.dart';
import 'package:flutter/material.dart';

class NotFoundScreen extends StatelessWidget {
  const NotFoundScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          context.l10n.notFound,
          style: Theme.of(context).textTheme.headlineLarge,
        ),
      ),
    );
  }
}
