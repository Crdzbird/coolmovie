import 'package:cool_movie/blocs/movie_detail/movie_detail_bloc.dart';
import 'package:cool_movie/extensions/build_context_extension.dart';
import 'package:cool_movie/presentation/add_review/add_review_screen.dart';
import 'package:cool_movie/presentation/movie_detail/widgets/reviews.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MovieDetailScreen extends StatelessWidget {
  const MovieDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movie Detail'),
      ),
      body: BlocBuilder<MovieDetailBloc, MovieDetailState>(
        buildWhen: (previous, current) {
          if (current is OnFailedState) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(current.error),
              ),
            );
            return false;
          }
          return true;
        },
        builder: (context, state) {
          if (state is OnLoadingState) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          }
          final success = state as OnSuccessState;
          return Column(
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(16),
                  bottomRight: Radius.circular(16),
                ),
                child: Image.network(
                  success.movieDetail.imgUrl,
                  height: MediaQuery.sizeOf(context).height * .2,
                  width: MediaQuery.sizeOf(context).width,
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(height: 6),
              Text(
                success.movieDetail.title,
                style: Theme.of(context).textTheme.titleMedium,
              ),
              const SizedBox(height: 6),
              Flexible(
                child: Reviews(reviews: success.movieDetail.movieReview),
              ),
            ],
          );
        },
      ),
      floatingActionButton: FloatingActionButton.small(
        child: const Icon(Icons.add),
        onPressed: () async {
          await showAdaptiveDialog<void>(
            context: context,
            builder: (builderContext) {
              return Dialog(
                child: AddReviewScreen(
                  id: context.routemaster.currentRoute.pathParameters['id'] ??
                      '',
                  movieDetailBloc: BlocProvider.of<MovieDetailBloc>(context),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
