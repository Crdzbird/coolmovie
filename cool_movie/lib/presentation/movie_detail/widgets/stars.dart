import 'package:flutter/material.dart';

class Stars extends StatelessWidget {
  const Stars({this.rating = 0, super.key});
  final int rating;

  @override
  Widget build(BuildContext context) {
    if (rating == 0) return const SizedBox.shrink();
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(
        rating,
        (index) {
          return const Icon(
            Icons.star,
          );
        },
      ),
    );
  }
}
