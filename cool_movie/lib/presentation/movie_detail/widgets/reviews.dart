import 'package:cool_movie/blocs/movie_detail/movie_detail_bloc.dart';
import 'package:cool_movie/models/movie_review/movie_review.dart';
import 'package:cool_movie/presentation/movie_detail/widgets/stars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Reviews extends StatelessWidget {
  const Reviews({required this.reviews, super.key});

  final List<MovieReview> reviews;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: reviews.length,
      itemBuilder: (context, index) {
        return Dismissible(
          key: Key(reviews[index].id),
          onDismissed: (_) {
            context.read<MovieDetailBloc>().deleteReview(reviews[index].id);
          },
          child: ListTile(
            isThreeLine: true,
            dense: true,
            leading: CircleAvatar(
              child: Text(
                reviews[index].user.name.substring(0, 1),
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
            title: Text(
              reviews[index].title,
              style: Theme.of(context).textTheme.titleMedium,
            ),
            subtitle: Text(
              reviews[index].body,
              style: Theme.of(context).textTheme.labelMedium,
            ),
            trailing: Stars(rating: reviews[index].rating),
          ),
        );
      },
    );
  }
}
