import 'package:cool_movie/blocs/movie_detail/movie_detail_bloc.dart';
import 'package:cool_movie/presentation/movie_detail/movie_detail_screen.dart';
import 'package:cool_movie/services/movie/usecase/movie_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MovieDetailProvider extends StatelessWidget {
  const MovieDetailProvider({required this.id, super.key});

  final String id;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (rContext) => MovieDetailBloc(
        movieService: rContext.read<MovieService>(),
      )..fetchMovieDetail(id),
      child: const MovieDetailScreen(),
    );
  }
}
