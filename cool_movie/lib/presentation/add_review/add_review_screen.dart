import 'package:cool_movie/blocs/movie_detail/movie_detail_bloc.dart';
import 'package:cool_movie/global/platform_button.dart';
import 'package:cool_movie/global/platform_textform.dart';
import 'package:cool_movie/l10n/l10n.dart';
import 'package:cool_movie/models/movie_review/movie_review.dart';
import 'package:cool_movie/models/user/user.dart';
import 'package:flutter/material.dart';

class AddReviewScreen extends StatefulWidget {
  const AddReviewScreen({
    required this.id,
    required this.movieDetailBloc,
    super.key,
  });
  final String id;
  final MovieDetailBloc movieDetailBloc;

  @override
  State<AddReviewScreen> createState() => _AddReviewScreenState();
}

class _AddReviewScreenState extends State<AddReviewScreen> {
  final titleController = TextEditingController();

  final bodyController = TextEditingController();

  final ratingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          PlatformTextForm(
            controller: titleController,
            label: context.l10n.title,
            filled: true,
          ),
          PlatformTextForm(
            controller: bodyController,
            label: context.l10n.body,
            filled: true,
          ),
          PlatformTextForm(
            controller: ratingController,
            label: context.l10n.rating,
            filled: true,
            keyboardType: TextInputType.number,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              PlatformButton(
                child: Text(context.l10n.cancel),
                onPressed: () => Navigator.pop(context),
              ),
              PlatformButton(
                child: Text(context.l10n.ok),
                onPressed: () {
                  widget.movieDetailBloc.addReview(
                    MovieReview(
                      id: widget.id,
                      title: titleController.text,
                      body: bodyController.text,
                      rating: int.parse(ratingController.text),
                      user: const User(
                        id: '7b4c31df-04b3-452f-a9ee-e9f8836013cc',
                        name: 'Marle',
                      ),
                    ),
                  );
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
