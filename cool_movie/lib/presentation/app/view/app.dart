import 'package:cool_movie/blocs/theme/theme_cubit.dart';
import 'package:cool_movie/extensions/build_context_extension.dart';
import 'package:cool_movie/l10n/l10n.dart';
import 'package:cool_movie/router/router_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:routemaster/routemaster.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: context.light,
      darkTheme: context.dark,
      themeMode: context.select((ThemeCubit cubit) => cubit.state).theme,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      routeInformationParser: const RoutemasterParser(),
      routerDelegate: routemasterDelegate,
    );
  }
}
