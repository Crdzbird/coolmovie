import 'package:cool_movie/blocs/theme/theme_cubit.dart';
import 'package:cool_movie/global/globals.dart';
import 'package:cool_movie/presentation/app/view/app.dart';
import 'package:cool_movie/services/graphql/client.dart';
import 'package:cool_movie/services/movie/usecase/movie_service.dart';
import 'package:cool_movie/services/preferences/preferences.dart';
import 'package:cool_movie/utils/dart_define.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Providers extends StatelessWidget {
  const Providers({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (_) =>
              Preferences(sharedPreferences: Globals.sharedPreferences),
        ),
        RepositoryProvider(create: (_) => Client(DartDefine().graphqlURL)),
        RepositoryProvider<MovieService>(
          create: (rContext) => MovieService(
            rContext.read<Client>(),
            rContext.read<Preferences>(),
          ),
        ),
      ],
      child: BlocProvider(
        create: (_) => ThemeCubit(),
        child: const App(),
      ),
    );
  }
}
