import 'dart:async';

import 'package:cool_movie/extensions/target_platform_extension.dart';
import 'package:cool_movie/models/movie/movie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ListTileMovie extends StatelessWidget {
  const ListTileMovie({
    required Movie movie,
    FutureOr<void> Function()? onTap,
    super.key,
  })  : _movie = movie,
        _onTap = onTap;
  final Movie _movie;
  final FutureOr<void> Function()? _onTap;

  @override
  Widget build(BuildContext context) {
    if (defaultTargetPlatform.isMacOS) {
      return CupertinoListTile.notched(
        leading: Image.network(
          _movie.imgUrl,
          width: 50,
          height: 50,
          fit: BoxFit.cover,
        ),
        title: Text(
          _movie.title,
          style: Theme.of(context).textTheme.titleMedium,
        ),
        subtitle: Text(
          _movie.id,
          style: Theme.of(context).textTheme.labelMedium,
        ),
        onTap: _onTap,
      );
    }
    return ListTile(
      leading: Image.network(
        _movie.imgUrl,
        width: 50,
        height: 50,
        fit: BoxFit.cover,
      ),
      title: Text(
        _movie.title,
        style: Theme.of(context).textTheme.titleMedium,
      ),
      subtitle: Text(
        _movie.id,
        style: Theme.of(context).textTheme.labelMedium,
      ),
      onTap: _onTap,
    );
  }
}
