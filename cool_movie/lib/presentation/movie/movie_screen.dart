import 'package:cool_movie/blocs/movie/movie_bloc.dart';
import 'package:cool_movie/extensions/build_context_extension.dart';
import 'package:cool_movie/presentation/movie/widgets/list_tile_movie.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MovieScreen extends StatelessWidget {
  const MovieScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          height: MediaQuery.sizeOf(context).height,
          width: MediaQuery.sizeOf(context).width,
          child: BlocBuilder<MovieBloc, MovieState>(
            builder: (builderContext, state) {
              if (state is OnLoadingState) {
                return const Center(
                  child: CircularProgressIndicator.adaptive(),
                );
              }
              final success = state as OnSuccessState;
              return ListView.builder(
                itemCount: success.movies.length,
                itemBuilder: (context, index) {
                  return ListTileMovie(
                    movie: success.movies[index],
                    onTap: () =>
                        context.routemaster.push(success.movies[index].id),
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
