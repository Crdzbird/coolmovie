import 'package:cool_movie/blocs/movie/movie_bloc.dart';
import 'package:cool_movie/presentation/movie/movie_screen.dart';
import 'package:cool_movie/services/movie/usecase/movie_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MovieProvider extends StatelessWidget {
  const MovieProvider({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (rContext) => MovieBloc(
        movieService: rContext.read<MovieService>(),
      )..fetchMovies(),
      child: const MovieScreen(),
    );
  }
}
