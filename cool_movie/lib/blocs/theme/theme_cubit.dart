import 'package:bloc/bloc.dart';
import 'package:cool_movie/models/enums/theme_type.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'theme_state.dart';

/// {@template theme_cubit}
/// A [Cubit] which manages the [ThemeMode].
/// {@endtemplate}
class ThemeCubit extends Cubit<ThemeState> {
  /// Creates a [ThemeCubit] with the initial [ThemeState].
  ThemeCubit() : super(ThemeState.initial());

  /// Toggles the [ThemeMode] to [ThemeMode.light].
  void toggleLightTheme() => emit(state.copyWith(ThemeMode.light));

  /// Toggles the [ThemeMode] to [ThemeMode.dark].
  void toggleDarkTheme() => emit(state.copyWith(ThemeMode.dark));

  /// Toggles the [ThemeMode] to [ThemeMode.system].
  void toggleSystemTheme() => emit(state.copyWith(ThemeMode.system));

  /// Changes the theme based on the given [theme].
  void changeTheme(String? theme) {
    if (theme == ThemeType.light.theme) return toggleLightTheme();
    if (theme == ThemeType.dark.theme) return toggleDarkTheme();
    if (theme == ThemeType.system.theme) return toggleSystemTheme();
  }
}
