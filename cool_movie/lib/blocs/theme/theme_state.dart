part of 'theme_cubit.dart';

/// The state of the [ThemeCubit].
class ThemeState extends Equatable {
  const ThemeState({
    required this.theme,
  });

  /// Creates a [ThemeState] from a [Map<String, dynamic>].
  factory ThemeState.fromMap(Map<String, dynamic> map) => ThemeState(
        theme: map['theme'] == 'system'
            ? ThemeMode.system
            : ThemeMode.values.firstWhere(
                (element) => element.toString() == 'ThemeMode.${map['theme']}',
              ),
      );

  /// Factory constructor for creating a new `ThemeState` instance.
  factory ThemeState.initial() => const ThemeState(theme: ThemeMode.system);
  final ThemeMode theme;

  /// Returns the props of the [ThemeState].
  @override
  List<Object?> get props => [theme];

  // ignore: lines_longer_than_80_chars
  /// Creates a copy of the [ThemeState] but with the given fields replaced with the new values.
  ThemeState copyWith(ThemeMode theme) => ThemeState(theme: theme);

  /// Returns a [String] representation of the [ThemeState].
  String enumToString() =>
      theme.toString().split('.').last.split(')').first.toUpperCase();

  /// Returns a [Map<String, dynamic>] representation of the [ThemeState].
  Map<String, String> get toMap => {'theme': theme.name};

  @override
  bool get stringify => true;
}
