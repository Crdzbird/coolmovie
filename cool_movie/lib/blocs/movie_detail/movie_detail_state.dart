part of 'movie_detail_bloc.dart';

abstract class MovieDetailState extends Equatable {
  const MovieDetailState();

  @override
  List<Object> get props => [];
}

class OnLoadingState extends MovieDetailState {}

class OnFailedState extends MovieDetailState {
  const OnFailedState(this.error);
  final String error;
}

class OnSuccessState extends MovieDetailState {
  const OnSuccessState({required this.movieDetail});
  final MovieDetail movieDetail;

  @override
  List<Object> get props => [movieDetail];
}
