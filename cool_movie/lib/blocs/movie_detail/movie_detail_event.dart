part of 'movie_detail_bloc.dart';

abstract class MovieDetailEvent extends Equatable {
  const MovieDetailEvent();
  @override
  List<Object> get props => [];
}

class OnSuccessEvent extends MovieDetailEvent {
  const OnSuccessEvent({
    required this.movieDetail,
  });
  final MovieDetail movieDetail;

  @override
  List<Object> get props => [movieDetail];
}

class OnFailureEvent extends MovieDetailEvent {
  const OnFailureEvent(this.error);
  final String error;
}

class OnLoadingEvent extends MovieDetailEvent {
  const OnLoadingEvent();
}
