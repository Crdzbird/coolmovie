import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cool_movie/extensions/failure_extension.dart';
import 'package:cool_movie/models/movie_detail/movie_detail.dart';
import 'package:cool_movie/models/movie_review/movie_review.dart';
import 'package:cool_movie/services/movie/usecase/movie_service.dart';
import 'package:equatable/equatable.dart';

part 'movie_detail_event.dart';
part 'movie_detail_state.dart';

class MovieDetailBloc extends Bloc<MovieDetailEvent, MovieDetailState> {
  MovieDetailBloc({
    required MovieService movieService,
  })  : _movieService = movieService,
        super(OnLoadingState()) {
    on<OnSuccessEvent>(
      (event, emit) => emit(
        OnSuccessState(movieDetail: event.movieDetail),
      ),
    );
    on<OnLoadingEvent>((_, emit) => emit(OnLoadingState()));
    on<OnFailureEvent>((event, emit) => emit(OnFailedState(event.error)));
  }

  final MovieService _movieService;

  MovieDetail get _movieDetail {
    if (state is OnSuccessState) {
      return (state as OnSuccessState).movieDetail;
    }
    return MovieDetail();
  }

  Future<void> fetchMovieDetail(String id) async {
    add(const OnLoadingEvent());
    final result = await _movieService.getMovieDetail(id);
    result.fold(
      (error) => add(OnFailureEvent(error.message)),
      (movieDetail) => add(OnSuccessEvent(movieDetail: movieDetail)),
    );
  }

  Future<void> deleteReview(String id) async {
    final result = await _movieService.removeReview(id);
    result.fold(
      (error) => add(OnFailureEvent(error.message)),
      (_) {
        final reviews = List<MovieReview>.from(_movieDetail.movieReview)
          ..removeWhere((element) => element.id == id);
        add(
          OnSuccessEvent(
            movieDetail: (state as OnSuccessState).movieDetail.copyWith(
                  movieReview: reviews,
                ),
          ),
        );
      },
    );
  }

  Future<void> addReview(MovieReview review) async {
    final result = await _movieService.addReview(review);
    result.fold(
      (error) {
        add(OnFailureEvent(error.message));
      },
      (_) {
        final reviews = List<MovieReview>.from(_movieDetail.movieReview)
          ..add(review);
        add(
          OnSuccessEvent(
            movieDetail: (state as OnSuccessState).movieDetail.copyWith(
                  movieReview: reviews,
                ),
          ),
        );
      },
    );
  }
}
