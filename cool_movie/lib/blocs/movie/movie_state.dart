part of 'movie_bloc.dart';

abstract class MovieState extends Equatable {
  const MovieState();

  @override
  List<Object> get props => [];
}

class OnLoadingState extends MovieState {}

class OnFailedState extends MovieState {
  const OnFailedState(this.error);
  final String error;
}

class OnSuccessState extends MovieState {
  const OnSuccessState({this.movies = const <Movie>[]});
  final List<Movie> movies;

  @override
  List<Object> get props => [movies];
}
