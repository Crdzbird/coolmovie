import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cool_movie/extensions/failure_extension.dart';
import 'package:cool_movie/models/movie/movie.dart';
import 'package:cool_movie/services/movie/usecase/movie_service.dart';
import 'package:equatable/equatable.dart';

part 'movie_event.dart';
part 'movie_state.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  MovieBloc({
    required MovieService movieService,
  })  : _movieService = movieService,
        super(OnLoadingState()) {
    on<OnSuccessEvent>(
      (event, emit) => emit(
        OnSuccessState(movies: event.movies),
      ),
    );
    on<OnLoadingEvent>((_, emit) => emit(OnLoadingState()));
    on<OnFailureEvent>((event, emit) => emit(OnFailedState(event.error)));
  }

  final MovieService _movieService;

  Future<void> fetchMovies() async {
    add(const OnLoadingEvent());
    final result = await _movieService.getMovies();
    result.fold(
      (error) => add(OnFailureEvent(error.message)),
      (movies) => add(OnSuccessEvent(movies: movies)),
    );
  }
}
