part of 'movie_bloc.dart';

abstract class MovieEvent extends Equatable {
  const MovieEvent();
  @override
  List<Object> get props => [];
}

class OnSuccessEvent extends MovieEvent {
  const OnSuccessEvent({
    required this.movies,
  });
  final List<Movie> movies;

  @override
  List<Object> get props => [movies];
}

class OnFailureEvent extends MovieEvent {
  const OnFailureEvent(this.error);
  final String error;
}

class OnLoadingEvent extends MovieEvent {
  const OnLoadingEvent();
}
