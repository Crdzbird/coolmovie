import 'package:cool_movie/models/enums/standard_color_type.dart';
import 'package:flex_seed_scheme/flex_seed_scheme.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Globals {
  static late SharedPreferences sharedPreferences;
  static final navigatorKey = GlobalKey<NavigatorState>();
  static final GlobalKey<ScaffoldMessengerState> rootScaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  // static late SharedPreferences sharedPreferences;
  static final schemeLight = SeedColorScheme.fromSeeds(
    primaryKey: StandardColorsType.brown.color,
    onPrimaryContainer: StandardColorsType.white.color,
    tones: const FlexTones.light(),
  );
  static final schemeDark = SeedColorScheme.fromSeeds(
    brightness: Brightness.dark,
    primaryKey: StandardColorsType.grey.color,
    onPrimaryContainer: StandardColorsType.greyLight.color,
    tones: const FlexTones.dark(),
  );
}
