import 'package:cool_movie/extensions/target_platform_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class PlatformButton extends StatelessWidget {
  const PlatformButton({
    required Widget child,
    super.key,
    VoidCallback? onPressed,
    double elevation = 0.0,
    BorderRadius borderRadius = const BorderRadius.all(Radius.circular(13)),
    bool isLoading = false,
    Color? backgroundColor,
    Color? foregroundColor,
    Color? disabledColor,
    EdgeInsetsGeometry? padding,
    EdgeInsetsGeometry? margin,
    Size? fixedSize,
  })  : _elevation = elevation,
        _borderRadius = borderRadius,
        _onPressed = onPressed,
        _isLoading = isLoading,
        _backgroundColor = backgroundColor,
        _foregroundColor = foregroundColor,
        _disabledColor = disabledColor,
        _padding = padding,
        _fixedSize = fixedSize,
        _margin = margin,
        _child = child;

  final double _elevation;
  final VoidCallback? _onPressed;
  final bool _isLoading;
  final Widget _child;
  final BorderRadius? _borderRadius;
  final Color? _backgroundColor;
  final Color? _foregroundColor;
  final Color? _disabledColor;
  final Size? _fixedSize;
  final EdgeInsetsGeometry? _padding;
  final EdgeInsetsGeometry? _margin;

  @override
  Widget build(BuildContext context) =>
      Padding(padding: _margin ?? EdgeInsets.zero, child: _buildButton());

  Widget _buildButton() {
    if (defaultTargetPlatform.isMacOS) {
      return SizedBox(
        width: _fixedSize?.width,
        height: _fixedSize?.height,
        child: CupertinoButton(
          borderRadius: _borderRadius,
          color: _backgroundColor,
          padding: _padding,
          disabledColor: _disabledColor ?? CupertinoColors.quaternarySystemFill,
          onPressed: _isLoading ? null : _onPressed,
          child:
              _isLoading ? const CircularProgressIndicator.adaptive() : _child,
        ),
      );
    }
    return FilledButton(
      onPressed: _isLoading ? null : _onPressed,
      style: ElevatedButton.styleFrom(
        elevation: _elevation,
        backgroundColor: _backgroundColor,
        foregroundColor: _foregroundColor,
        padding: _padding,
        fixedSize: _fixedSize,
        shape: RoundedRectangleBorder(
          borderRadius: _borderRadius ?? BorderRadius.circular(8),
        ),
        enableFeedback: true,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      child: _isLoading ? const CircularProgressIndicator.adaptive() : _child,
    );
  }
}
