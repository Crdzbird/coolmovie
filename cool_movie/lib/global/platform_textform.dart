import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PlatformTextForm extends StatelessWidget {
  const PlatformTextForm({
    super.key,
    this.borderRadius = BorderRadius.zero,
    this.inputFormatters,
    this.hintText,
    this.controller,
    this.keyboardType,
    this.labelPadding = EdgeInsets.zero,
    this.textInputAction,
    this.obscureText = false,
    this.expands = false,
    this.enabled = true,
    this.filled = false,
    this.onEditingComplete,
    this.suffixIcon,
    this.prefixIcon,
    this.decoration,
    this.borderSide = BorderSide.none,
    this.label,
  });
  final List<TextInputFormatter>? inputFormatters;
  final BorderRadius borderRadius;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final VoidCallback? onEditingComplete;
  final String? hintText;
  final String? label;
  final EdgeInsetsGeometry labelPadding;
  final TextInputAction? textInputAction;
  final bool obscureText;
  final bool expands;
  final bool enabled;
  final bool? filled;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final BorderSide borderSide;
  final InputDecoration? decoration;

  @override
  Widget build(BuildContext context) {
    final commonDecoration = decoration ??
        InputDecoration(
          alignLabelWithHint: true,
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          labelText: label,
          filled: true,
          suffixIcon: suffixIcon,
          prefixIcon: prefixIcon,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          hintText: hintText,
          isDense: true,
        );
    return TextFormField(
      controller: controller,
      keyboardType: keyboardType,
      textInputAction: textInputAction,
      inputFormatters: inputFormatters,
      obscureText: obscureText,
      expands: expands,
      enableInteractiveSelection: true,
      enabled: enabled,
      decoration: commonDecoration,
      autovalidateMode: AutovalidateMode.onUserInteraction,
    );
  }
}
