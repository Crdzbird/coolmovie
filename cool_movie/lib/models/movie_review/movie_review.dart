import 'dart:convert';

import 'package:cool_movie/models/user/user.dart';
import 'package:equatable/equatable.dart';

/// A class representing a movie.
///
/// This class contains various properties to describe a movie, such as its id, title, body.
class MovieReview extends Equatable {
  /// Default constructor for creating a [MovieReview] instance.
  const MovieReview({
    this.id = '',
    this.title = '',
    this.body = '',
    this.rating = 0,
    this.user = const User(),
  });

  /// Named constructor for creating a [MovieReview] instance from a map of values.
  factory MovieReview.fromMap(Map<String, dynamic> data) => MovieReview(
        id: data['id'] as String? ?? '',
        title: data['title'] as String? ?? '',
        body: data['body'] as String? ?? '',
        rating: data['rating'] as int? ?? 0,
        user: User.fromMap(
          data['userByUserReviewerId'] as Map<String, dynamic>? ?? {},
        ),
      );

  /// Named constructor for creating a [MovieReview] instance from a JSON string.
  ///
  /// Parses the string and returns the resulting Json object as [MovieReview].
  factory MovieReview.fromJson(String data) {
    return MovieReview.fromMap(json.decode(data) as Map<String, dynamic>);
  }

  static List<MovieReview> fromJsonList(Map<String, dynamic> json) {
    if (json.isEmpty) return [];
    final movieNodes = json['movieReviewsByMovieId']['nodes'] as List<Object?>;
    return movieNodes
        .map((movie) => MovieReview.fromMap(movie! as Map<String, dynamic>))
        .toList();
  }

  static String movieListToJson(List<MovieReview> movies) {
    final movieJsonList = movies.map((movie) => movie.toJson).toList();
    final jsonData = <String, dynamic>{
      'movieReviewsByMovieId': {'nodes': movieJsonList},
    };
    return jsonEncode(jsonData);
  }

  /// Static method to create a JSON string for creating a movie.
  ///
  /// Takes various parameters for a movie and returns a JSON-encoded string suitable for creating a movie via an API.
  static String creatMovieReviewToJson({
    String id = '',
    String title = '',
    String body = '',
    int rating = 0,
    User user = const User(),
  }) =>
      jsonEncode({
        'id': id,
        'title': title,
        'body': body,
        'rating': rating,
        'user': user.toMap,
      });

  final String id;
  final String title;
  final String body;
  final int rating;
  final User user;

  /// Converts the current [MovieReview] instance to a map.
  ///
  /// Useful for serializing the group data into a map format.
  Map<String, dynamic> get toMap => {
        'id': id,
        'title': title,
        'body': body,
        'rating': rating,
        'user': user.toMap,
      };

  Map<String, dynamic> get toMapCreate => {
        'movieReview': {
          'title': title,
          'body': body,
          'rating': rating,
          'movieId': id,
          'userReviewerId': user.id,
        },
      };

  /// Converts [MovieReview] to a JSON string.
  ///
  /// Serializes the group data into a JSON string format.
  String get toJson => json.encode(toMap);

  /// Creates a copy of the current [MovieReview] instance with updated fields.
  ///
  /// Allows creating a new instance with modified group data while keeping other fields unchanged.
  MovieReview copyWith({
    String? id,
    String? title,
    String? body,
    int? rating,
    User? user,
  }) {
    return MovieReview(
      id: id ?? this.id,
      title: title ?? this.title,
      body: body ?? this.body,
      rating: rating ?? this.rating,
      user: user ?? this.user,
    );
  }

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      title,
      body,
      rating,
      user,
    ];
  }
}
