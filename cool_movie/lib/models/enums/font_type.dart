/// An enumeration of font types.
///
/// This enum is utilized for specifying different types of fonts used within the application.
/// Currently, it includes only one font type, but it can be expanded to include more fonts in the future.
enum FontType {
  /// Font type for 'sfPro'.
  ///
  /// This is represented by the string 'SFPro', which can be used to specify the font in text styling.
  sfPro('SFPro');

  /// Constructs a [FontType] instance with the given [name].
  ///
  /// The [name] is a string representation of the font type, which corresponds to the font's name.
  const FontType(this.name);

  /// The name of the font type.
  ///
  /// This string can be used when specifying fonts in text style settings.
  final String name;
}
