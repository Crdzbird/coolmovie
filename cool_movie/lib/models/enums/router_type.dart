enum RouterType {
  initScreen('/'),
  splashScreen('/splash'),
  movieScreen('/movie'),
  movieDetailScreen('/movie/:id'),
  notFoundScreen('/404');

  const RouterType(this.path);
  final String path;
}
