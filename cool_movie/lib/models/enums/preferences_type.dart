enum PreferencesType {
  movie('movie'),
  movieDetail('movieDetail'),
  movies('movies');

  const PreferencesType(this.name);
  final String name;
}
