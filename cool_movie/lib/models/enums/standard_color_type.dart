import 'package:flutter/material.dart';

/// An enumeration of custom colors used in the app.
///
/// Each enum value represents a specific color with a predefined ARGB value.
enum StandardColorsType {
  /// Black color with ARGB value of 0xFF000000.
  black(Color(0xFF000000)),
  blackSemiTranslucid(Color(0x02000000)),

  /// White color with ARGB value of 0xFFFFFFFF.
  white(Color(0xFFFFFFFF)),

  /// Grey color with ARGB value of 0xFF9E9E9E.
  grey(Color(0xFF9E9E9E)),

  /// Grey color with ARGB value of 0xFFE0E0E0.
  greyLight(Color(0xFFE0E0E0)),

  /// Redish color with ARGB value of 0x28FFFFFF.
  redish(Color(0x28FFFFFF)),

  /// DarkBlue color with ARGB value of 0xB1214ECC.
  darkBlue(Color(0xB1214ECC)),

  /// Transparent color with ARGB value of 0x00FFFFFF.
  transparent(Color(0x00FFFFFF)),

  /// Brown color with ARGB value of 0x13FFFFFF.
  brown(Color(0x13FFFFFF));

  /// Creates a [StandardColorsType] instance with the given [color].
  ///
  /// [color] is a [Color] instance representing the ARGB value of the color.
  const StandardColorsType(this.color);

  /// The [Color] value of the enum item.
  ///
  /// This is the actual Flutter [Color] object that can be used in the UI.
  final Color color;
}
