/// An enumeration of theme types for the UI.
///
/// This enum is used to define and manage different UI themes, such as light, dark, and system themes,
/// within the application. It facilitates theme switching and ensures a consistent approach to handling UI themes.
enum ThemeType {
  /// Light theme type.
  ///
  /// Represented by the string 'light', this theme type is associated with a brighter UI appearance,
  /// typically with lighter colors and backgrounds.
  light('light'),

  /// Dark theme type.
  ///
  /// Represented by the string 'dark', this theme type is associated with a darker UI appearance,
  /// typically with darker colors and backgrounds.
  dark('dark'),

  /// System default theme type.
  ///
  /// Represented by the string 'system', this theme type aligns with the system's default theme settings,
  /// automatically adapting to light or dark mode based on system preferences.
  system('system');

  /// Constructs a [ThemeType] instance with the given [theme].
  ///
  /// The [theme] is a string representation of the UI theme type.
  const ThemeType(this.theme);

  /// The string representation of the UI theme.
  ///
  /// This can be used to easily switch and manage themes within the application.
  final String theme;

  /// A list of available theme types defined in [ThemeType].
  ///
  /// This static getter provides easy access to all defined themes, facilitating operations like theme selection or preferences.
  static List<String> get themes => [
        ThemeType.light.theme,
        ThemeType.dark.theme,
        ThemeType.system.theme,
      ];
}
