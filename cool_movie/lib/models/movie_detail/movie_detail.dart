import 'dart:convert';

import 'package:cool_movie/models/movie_review/movie_review.dart';
import 'package:equatable/equatable.dart';

/// A class representing a movie.
///
/// This class contains various properties to describe a movie, such as its id, title, imgUrl.
class MovieDetail extends Equatable {
  /// Default constructor for creating a [MovieDetail] instance.
  MovieDetail({
    this.id = '',
    this.title = '',
    this.imgUrl = '',
    DateTime? realaseDate,
    this.movieReview = const [],
  }) : releaseDate = realaseDate ?? DateTime.now();

  /// Named constructor for creating a [MovieDetail] instance from a map of values.
  factory MovieDetail.fromMap(Map<String, dynamic> data) => MovieDetail(
        id: data['id'] as String? ?? '',
        title: data['title'] as String? ?? '',
        imgUrl: data['imgUrl'] as String? ?? '',
        realaseDate: data['releaseDate'] == null
            ? null
            : DateTime.parse(data['releaseDate'] as String),
        movieReview: MovieReview.fromJsonList(data),
      );

  /// Named constructor for creating a [MovieDetail] instance from a JSON string.
  ///
  /// Parses the string and returns the resulting Json object as [MovieDetail].
  factory MovieDetail.fromJson(String data) {
    return MovieDetail.fromMap(json.decode(data) as Map<String, dynamic>);
  }

  static List<MovieDetail> fromJsonList(Map<String, dynamic> json) {
    if (json.isEmpty) return [];
    final movieNodes = json['allMovieDetails']['nodes'] as List<Object?>;
    return movieNodes
        .map((movie) => MovieDetail.fromMap(movie! as Map<String, dynamic>))
        .toList();
  }

  static String movieListToJson(List<MovieDetail> movies) {
    final movieJsonList = movies.map((movie) => movie.toJson).toList();
    final jsonData = <String, dynamic>{
      'allMovieDetails': {'nodes': movieJsonList},
    };
    return jsonEncode(jsonData);
  }

  /// Static method to create a JSON string for creating a movie.
  ///
  /// Takes various parameters for a movie and returns a JSON-encoded string suitable for creating a movie via an API.
  static String creatMovieDetailToJson({
    String id = '',
    String title = '',
    String imgUrl = '',
    DateTime? releaseDate,
    List<MovieReview> movieReview = const [],
  }) =>
      jsonEncode({
        'id': id,
        'title': title,
        'imgUrl': imgUrl,
        'releaseDate': releaseDate,
        'movieReview': MovieReview.movieListToJson(movieReview),
      });

  final String id;
  final String title;
  final String imgUrl;
  final DateTime releaseDate;
  final List<MovieReview> movieReview;

  /// Converts the current [MovieDetail] instance to a map.
  ///
  /// Useful for serializing the group data into a map format.
  Map<String, dynamic> get toMap => {
        'id': id,
        'title': title,
        'imgUrl': imgUrl,
        'releaseDate': releaseDate,
        'movieReview': MovieReview.movieListToJson(movieReview),
      };

  /// Converts [MovieDetail] to a JSON string.
  ///
  /// Serializes the group data into a JSON string format.
  String get toJson => json.encode(toMap);

  /// Creates a copy of the current [MovieDetail] instance with updated fields.
  ///
  /// Allows creating a new instance with modified group data while keeping other fields unchanged.
  MovieDetail copyWith({
    String? id,
    String? title,
    String? imgUrl,
    DateTime? releaseDate,
    List<MovieReview>? movieReview,
  }) {
    return MovieDetail(
      id: id ?? this.id,
      title: title ?? this.title,
      imgUrl: imgUrl ?? this.imgUrl,
      realaseDate: releaseDate ?? this.releaseDate,
      movieReview: movieReview ?? this.movieReview,
    );
  }

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      title,
      imgUrl,
      releaseDate,
      movieReview,
    ];
  }
}
