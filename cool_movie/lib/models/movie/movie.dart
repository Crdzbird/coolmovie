import 'dart:convert';

import 'package:equatable/equatable.dart';

/// A class representing a movie.
///
/// This class contains various properties to describe a movie, such as its id, title, imgUrl.
class Movie extends Equatable {
  /// Default constructor for creating a [Movie] instance.
  const Movie({
    this.id = '',
    this.title = '',
    this.imgUrl = '',
  });

  /// Named constructor for creating a [Movie] instance from a map of values.
  factory Movie.fromMap(Map<String, dynamic> data) => Movie(
        id: data['id'] as String? ?? '',
        title: data['title'] as String? ?? '',
        imgUrl: data['imgUrl'] as String? ?? '',
      );

  /// Named constructor for creating a [Movie] instance from a JSON string.
  ///
  /// Parses the string and returns the resulting Json object as [Movie].
  factory Movie.fromJson(String data) {
    return Movie.fromMap(json.decode(data) as Map<String, dynamic>);
  }

  static List<Movie> fromJsonList(Map<String, dynamic> json) {
    print(json);
    if (json.isEmpty) return [];
    final movieNodes = json['allMovies']['nodes'] as List<Object?>;
    return movieNodes
        .map((movie) => Movie.fromMap(movie! as Map<String, dynamic>))
        .toList();
  }

  static String movieListToJson(List<Movie> movies) {
    final movieJsonList = movies.map((movie) => movie.toJson).toList();
    final jsonData = <String, dynamic>{
      'allMovies': {'nodes': movieJsonList},
    };
    return jsonEncode(jsonData);
  }

  /// Static method to create a JSON string for creating a movie.
  ///
  /// Takes various parameters for a movie and returns a JSON-encoded string suitable for creating a movie via an API.
  static String creatMovieToJson({
    String id = '',
    String title = '',
    String imgUrl = '',
  }) =>
      jsonEncode({
        'id': id,
        'title': title,
        'imgUrl': imgUrl,
      });

  final String id;
  final String title;
  final String imgUrl;

  /// Converts the current [Movie] instance to a map.
  ///
  /// Useful for serializing the group data into a map format.
  Map<String, dynamic> get toMap => {
        'id': id,
        'title': title,
        'imgUrl': imgUrl,
      };

  /// Converts [Movie] to a JSON string.
  ///
  /// Serializes the group data into a JSON string format.
  String get toJson => json.encode(toMap);

  /// Creates a copy of the current [Movie] instance with updated fields.
  ///
  /// Allows creating a new instance with modified group data while keeping other fields unchanged.
  Movie copyWith({
    String? id,
    String? title,
    String? imgUrl,
  }) {
    return Movie(
      id: id ?? this.id,
      title: title ?? this.title,
      imgUrl: imgUrl ?? this.imgUrl,
    );
  }

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      title,
      imgUrl,
    ];
  }
}
