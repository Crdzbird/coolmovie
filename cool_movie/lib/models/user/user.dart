import 'dart:convert';

import 'package:equatable/equatable.dart';

/// A class representing a movie.
///
/// This class contains various properties to describe a movie, such as its id, name, imgUrl.
class User extends Equatable {
  /// Default constructor for creating a [User] instance.
  const User({
    this.id = '',
    this.name = '',
  });

  /// Named constructor for creating a [User] instance from a map of values.
  factory User.fromMap(Map<String, dynamic> data) => User(
        id: data['id'] as String? ?? '',
        name: data['name'] as String? ?? '',
      );

  /// Named constructor for creating a [User] instance from a JSON string.
  ///
  /// Parses the string and returns the resulting Json object as [User].
  factory User.fromJson(String data) {
    return User.fromMap(json.decode(data) as Map<String, dynamic>);
  }

  static List<User> fromJsonList(Map<String, dynamic> json) {
    if (json.isEmpty) return [];
    final movieNodes = json['allUsers']['nodes'] as List<Object?>;
    return movieNodes
        .map((movie) => User.fromMap(movie! as Map<String, dynamic>))
        .toList();
  }

  static String movieListToJson(List<User> movies) {
    final movieJsonList = movies.map((movie) => movie.toJson).toList();
    final jsonData = <String, dynamic>{
      'allUsers': {'nodes': movieJsonList},
    };
    return jsonEncode(jsonData);
  }

  /// Static method to create a JSON string for creating a movie.
  ///
  /// Takes various parameters for a movie and returns a JSON-encoded string suitable for creating a movie via an API.
  static String creatUserToJson({
    String id = '',
    String name = '',
  }) =>
      jsonEncode({
        'id': id,
        'name': name,
      });

  final String id;
  final String name;

  /// Converts the current [User] instance to a map.
  ///
  /// Useful for serializing the group data into a map format.
  Map<String, dynamic> get toMap => {
        'id': id,
        'name': name,
      };

  /// Converts [User] to a JSON string.
  ///
  /// Serializes the group data into a JSON string format.
  String get toJson => json.encode(toMap);

  /// Creates a copy of the current [User] instance with updated fields.
  ///
  /// Allows creating a new instance with modified group data while keeping other fields unchanged.
  User copyWith({
    String? id,
    String? name,
  }) {
    return User(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      name,
    ];
  }
}
