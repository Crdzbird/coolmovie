import 'package:cool_movie/global/globals.dart';
import 'package:cool_movie/models/enums/router_type.dart';
import 'package:cool_movie/presentation/movie/provider/movie_provider.dart';
import 'package:cool_movie/presentation/movie_detail/provider/movie_detail_provider.dart';
import 'package:cool_movie/presentation/not_found/not_found_screen.dart';
import 'package:cool_movie/presentation/splash/splash_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:routemaster/routemaster.dart';

final routemasterDelegate = RoutemasterDelegate(
  navigatorKey: Globals.navigatorKey,
  routesBuilder: _routeBuilder,
);

RouteMap _routeBuilder(BuildContext context) {
  return RouteMap(
    routes: {
      RouterType.initScreen.path: (routeData) {
        return TransitionPage<Widget>(
          child: const SplashScreen(),
          popTransition: PageTransition.platformDefault(defaultTargetPlatform),
          pushTransition: PageTransition.platformDefault(defaultTargetPlatform),
          name: RouterType.initScreen.path,
          key: ValueKey(RouterType.initScreen.path),
          restorationId: RouterType.initScreen.path,
        );
      },
      RouterType.movieScreen.path: (routeData) {
        return TransitionPage<Widget>(
          child: const MovieProvider(),
          popTransition: PageTransition.platformDefault(defaultTargetPlatform),
          pushTransition: PageTransition.platformDefault(defaultTargetPlatform),
          name: RouterType.movieScreen.path,
          key: ValueKey(RouterType.movieScreen.path),
          restorationId: RouterType.movieScreen.path,
        );
      },
      RouterType.movieDetailScreen.path: (routeData) {
        return TransitionPage<Widget>(
          child: MovieDetailProvider(
            id: routeData.pathParameters['id'] ?? '',
          ),
          popTransition: PageTransition.platformDefault(defaultTargetPlatform),
          pushTransition: PageTransition.platformDefault(defaultTargetPlatform),
          name: RouterType.movieDetailScreen.path,
          key: ValueKey(RouterType.movieDetailScreen.path),
          restorationId: RouterType.movieDetailScreen.path,
        );
      },
    },
    onUnknownRoute: (routeData) {
      return TransitionPage<Widget>(
        child: const NotFoundScreen(),
        popTransition: PageTransition.platformDefault(defaultTargetPlatform),
        pushTransition: PageTransition.platformDefault(defaultTargetPlatform),
        name: RouterType.notFoundScreen.path,
        key: ValueKey(RouterType.notFoundScreen.path),
        restorationId: RouterType.notFoundScreen.path,
      );
    },
  );
}
