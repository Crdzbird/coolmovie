import 'package:cool_movie/bootstrap.dart';
import 'package:cool_movie/presentation/app/provider/providers.dart';

void main() {
  bootstrap(() => const Providers());
}
