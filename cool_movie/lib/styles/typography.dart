import 'package:cool_movie/models/enums/standard_color_type.dart';
import 'package:flutter/material.dart';

/// Tipography used when the app is in [Light Mode].
final typographyLightTheme = TextTheme(
  displayLarge: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 57,
  ),
  displayMedium: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 45,
  ),
  displaySmall: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 36,
  ),
  headlineLarge: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 32,
  ),
  headlineMedium: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 28,
  ),
  headlineSmall: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 24,
  ),
  titleLarge: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 22,
  ),
  titleMedium: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 16,
  ),
  titleSmall: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 14,
  ),
  bodyLarge: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 16,
  ),
  bodyMedium: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 14,
  ),
  bodySmall: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 12,
  ),
  labelLarge: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 14,
  ),
  labelMedium: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 12,
  ),
  labelSmall: TextStyle(
    color: StandardColorsType.black.color,
    fontSize: 11,
  ),
);

/// Tipography used when the app is in [Dark Mode].
final typographyDarkTheme = TextTheme(
  displayLarge: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 57,
  ),
  displayMedium: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 45,
  ),
  displaySmall: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 36,
  ),
  headlineLarge: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 32,
  ),
  headlineMedium: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 28,
  ),
  headlineSmall: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 24,
  ),
  titleLarge: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 22,
  ),
  titleMedium: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 16,
  ),
  titleSmall: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 14,
  ),
  bodyLarge: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 16,
  ),
  bodyMedium: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 14,
  ),
  bodySmall: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 12,
  ),
  labelLarge: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 14,
  ),
  labelMedium: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 12,
  ),
  labelSmall: TextStyle(
    color: StandardColorsType.white.color,
    fontSize: 11,
  ),
);
